﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uprazhnenija
{
    class Program
    {
        // TODO: Главный метод
        static void Main(string[] args)
        {
            int[] mas = CreateMas();
            FillMasRandom(mas);
            Console.WriteLine();
            PrintMas(mas);
            Console.WriteLine("\nСумма элементов массива = " + SummaMas(mas));
            Console.WriteLine("Произведение элементов массива = " + ProizvedMas(mas));
            Console.WriteLine("Минимальный элемент массива = " + MinMas(mas));
            Console.WriteLine("Максимальный элемент массива = " + MaxMas(mas));
            Console.WriteLine("Индекс искомого числа = " +
                KNumberMas(mas, VvodNumberSearch("Введите число для поиска: ")));

            UvelichMas(mas, VvodNumberSearch("Введите число для увеличения массива: "));
            PrintMas(mas);

            Console.WriteLine();

            ReversMas(mas);
            PrintMas(mas);

            Console.ReadKey();
        }

        // TODO: Сумма элементов массива
        static int SummaMas (int[] mas)
        {
            int summa;
            summa = 0;

            for (int i = 0; i < mas.Length; i++)
            {
                summa = summa + mas[i];
            }

            return summa;
        }
        // TODO: Произведение элементов массива
        static long ProizvedMas(int[] mas)
        {
            long proizv;
            proizv = 1;

            for (int i = 0; i < mas.Length; i++)
            {
                proizv = proizv * mas[i];
            }

            return proizv;
        }
        // TODO: Поиск минимума
        static int MinMas(int[] mas)
        {
            int min;
            min = mas[0];

            for (int i = 0; i < mas.Length; i++)
            {
                if (min > mas[i])
                {
                    min = mas[i];
                }
            }

            return min;
        }
        // TODO: Поиск максимума
        static int MaxMas(int[] mas)
        {
            int max;
            max = mas[0];

            for (int i = 0; i < mas.Length; i++)
            {
                if (max < mas[i])
                {
                    max = mas[i];
                }
            }

            return max;
        }
        // TODO: Ввод к-го значения
        static int VvodNumberSearch( string message)
        {
            int k;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out k);

            } while (check == false);
            return k;
        }
        // TODO: Поиск индекса К-го значения
        static int KNumberMas(int[] mas, int k)
        {        
            for (int i = 0; i < mas.Length; i++)
            {
                if (mas[i] == k)
                {
                    return i;
                }
            }

            return -1;
        }
        // TODO: Увеличение всех элементов на к-ое
        static void UvelichMas(int[] mas, int k)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] *= k;
            }
        }
        // TODO: Реверс массива
        static void ReversMas(int[] mas)
        {
            int bufer;

            for (int i = 0; i < (mas.Length / 2); i++)
            {
                bufer = mas[i];
                mas[i] = mas[mas.Length - 1 - i];
                mas[mas.Length - 1 - i] = bufer;
            }

        }
        // TODO: Заполнение массива псевдослучайными числами
        static void FillMasRandom(int[] mas)
        {
            Random rnd = new Random();

            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(0, 100);
            }
        }
        // TODO: Вывод массива
        static void PrintMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write(mas[i] + " ");
            }
        }
        // TODO: Задание размерности массива
        static int[] CreateMas()
        {
            Console.Write("Input n: ");
            int n = Convert.ToInt32(Console.ReadLine());

            return new int[n];
        }


    }
}
